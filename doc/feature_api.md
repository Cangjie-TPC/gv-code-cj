## vcode库

### 介绍
一个带干扰线和干扰点的字母数字验证码组件

### 设置验证码组件样式

#### 1.1 主要接口参数
```cangjie
@Component
public class VCodeComponent {
    // 验证码范围
    var allCode: String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    // 数字验证码范围
    var numberCode: String = "0123456789"
    // 验证码长度，默认值 6
    var num: Int64 = 6
    // 画布宽度，默认值 120
    var width: Int64 = 120
    // 画布高度，默认值 40
    var height: Int64 = 40
    // 画布背景色
    var bgColor: UInt32 = 0X800080
    // 画布字体颜色
    var fontColor: UInt32 = 0XFFFFFF
    // 自定义颜色 默认false
    var cuntomColor: Bool = false
    // 自定义数字验证码 默认false
    var cuntomNumber: Bool = false
    // 自定义按钮 默认false
    var customButton: Bool = false
    // 是否显示干扰点，默认 true
    var hasPoint: Bool = true
    // 是否显示干扰线，默认 true
    var hasLine: Bool = true
    // 显示指定验证码
    var curVCode: String = ""
    // 回调验证码
    @Link
    var callBackCode: String
    ...
}
```

#### 1.2 示例

```cangjie
internal import ohos.base.*
internal import ohos.component.*
internal import ohos.state_manage.*
import ohos.state_macro_manage.Entry
import ohos.state_macro_manage.Component
import ohos.state_macro_manage.State
import vcodelib.*

@Entry
@Component
class EntryView {
    @State
    var callBackCode: String = ""

    func build() {
        Column(5) {
            Text("无干扰线、无干扰点").fontSize(16)
            VCodeComponent(hasPoint: false, hasLine: false, callBackCode: "")

            Text("有干扰线、无干扰点").fontSize(16)
            VCodeComponent(hasPoint: false, callBackCode: "")

            Text("有干扰线、有干扰点").fontSize(16)
            VCodeComponent(callBackCode: "")

            Text("回调当前验证码值 ${callBackCode}").fontSize(16)
            VCodeComponent(callBackCode: callBackCode)

            Text("外部主动刷新验证码").fontSize(16)
            VCodeComponent(callBackCode: "", customButton: true)

            Text("自定义紫色背景、白色字体").fontSize(16)
            VCodeComponent(callBackCode: "", cuntomColor: true, bgColor: 0X800080, fontColor: 0XFFFFFF)

            Text("纯数字验证码").fontSize(16)
            VCodeComponent(callBackCode: "", cuntomNumber: true)

            Text("显示指定验证码dpY7U9").fontSize(16)
            VCodeComponent(callBackCode: "", curVCode: "dpY7U9")
        }.alignItems(HorizontalAlign.Center).width(100.percent).height(100.percent)
    }
}
```

