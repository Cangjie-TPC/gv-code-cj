<div align="center">
<h1>gv-code-cj</h1>
</div>


<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.53.13-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-NA-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## 介绍

gv-code-cj是一个带干扰线和干扰点的字母数字验证码组件
<p align="left">
<img src="doc/assets/vcode.png"  width="30%">
</p>

## 特性

- 支持设置验证码组件展示样式

### 源码目录

```shell
.
├── AppScope
├── doc    # API文档和使用手册存放目录
│   ├── assets
│   ├── feature_api.md
├── entry  # 示例代码文件夹 
├── hvigor  # 构建工具目录
├── CHANGELOG.md
├── LICENSE
├── README.md  # 介绍及使用说明 
```

## 接口说明

主要是核心类和函数接口说明见 [API](doc/feature_api.md)<!--待更新-->

## 使用说明

1. 通过 module 引入
    1. 克隆下载项目
    2. 将 vcode4cj 模块拷贝到应用项目下
    3. 修改自身应用 entry/src/main/cangjie 下的 cjpm.toml 文件，在 [dependencies] 字段下添加 dependencies.vcodelib ,再添加path = "../../../../vcodelib/src/main/cangjie"
       ```toml
       [dependencies]
          [dependencies.vcodelib]
             path = "../../../../vcodelib/src/main/cangjie"
       ```
    4. 在项目中使用 import vcodelib.* 引用 vcodelib 
       ```cangjie
       import vcodelib.*
       ```
2. 把 vcode4cj 作为三方库依赖引入
    1. 目标工程把 vcode4cj 依赖库作为 git submodule 引入

    ```
    > cd $工程根目录
    > mkdir third-party
    > cd third-party
    > git submodule add "https://gitcode.com/Cangjie-TPC/vcode4cj.git"
    ```
    2. 修改自身应用 entry/src/main/cangjie 下的 cjpm.toml 文件，添加依赖
    ```toml
   [dependencies]
      [dependencies.vcodelib]
         path = "../../../../vcodelib/src/main/cangjie"    
   ```
    3. 在项目中使用 circleimageview 组件
       ```cangjie
       import vcodelib.*
       ```

## 功能示例

详见 `entry`

```cangjie
internal import ohos.base.*
internal import ohos.component.*
internal import ohos.state_manage.*
import ohos.state_macro_manage.Entry
import ohos.state_macro_manage.Component
import ohos.state_macro_manage.State
import vcodelib.*

@Entry
@Component
class EntryView {
    @State
    var callBackCode: String = ""

    func build() {
        Column(5) {
            Text("无干扰线、无干扰点").fontSize(16)
            VCodeComponent(hasPoint: false, hasLine: false, callBackCode: "")

            Text("有干扰线、无干扰点").fontSize(16)
            VCodeComponent(hasPoint: false, callBackCode: "")

            Text("有干扰线、有干扰点").fontSize(16)
            VCodeComponent(callBackCode: "")

            Text("回调当前验证码值 ${callBackCode}").fontSize(16)
            VCodeComponent(callBackCode: callBackCode)

            Text("外部主动刷新验证码").fontSize(16)
            VCodeComponent(callBackCode: "", customButton: true)

            Text("自定义紫色背景、白色字体").fontSize(16)
            VCodeComponent(callBackCode: "", cuntomColor: true, bgColor: 0X800080, fontColor: 0XFFFFFF)

            Text("纯数字验证码").fontSize(16)
            VCodeComponent(callBackCode: "", cuntomNumber: true)

            Text("显示指定验证码dpY7U9").fontSize(16)
            VCodeComponent(callBackCode: "", curVCode: "dpY7U9")
        }.alignItems(HorizontalAlign.Center).width(100.percent).height(100.percent)
    }
}
```


## 约束与限制

在下述版本验证通过：

    DevEco Studio NEXT Developer Beta2(5.0.3.910) 
    Cangjie Support Plugin 5.0.3.906 
## 开源协议

本项目基于 [The MIT License](./LICENSE) ，请自由的享受和参与开源。

## 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。

